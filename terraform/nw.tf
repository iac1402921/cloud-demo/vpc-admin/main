#-----------
## Get ext-network
#-----------
data "openstack_networking_network_v2" "ext-network" {
  name = "ext-network"
}

#-----------
## Create external-router
#-----------
resource "openstack_networking_router_v2" "external-router" {
  name                = "external-router"
  admin_state_up      = "true"
  enable_snat         = "false"
  external_network_id = data.openstack_networking_network_v2.ext-network.id
}

#-----------
## Create int-network
#-----------
resource "openstack_networking_network_v2" "int-network" {
  name           = "int-network"
  admin_state_up = "true"
}

#-----------
## Create int-subnet-1
#-----------
resource "openstack_networking_subnet_v2" "int-subnet-1" {
  network_id = openstack_networking_network_v2.int-network.id
  name       = "int-subnet-1"
  cidr       = "10.10.1.0/24"
  gateway_ip = "10.10.1.1"
  dns_nameservers = ["192.168.2.254", "192.168.5.254"]
}

#-----------
## Create int-interface
#-----------
resource "openstack_networking_router_interface_v2" "int-interface" {
  router_id = openstack_networking_router_v2.external-router.id
  subnet_id = openstack_networking_subnet_v2.int-subnet-1.id
}
