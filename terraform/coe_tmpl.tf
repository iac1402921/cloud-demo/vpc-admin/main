#-----------
## Create Cluster Template k8s-1.26.7
#-----------
resource "openstack_containerinfra_clustertemplate_v1" "k8s-1-26-7" {
  name                  = "k8s-1.26.7"
  image                 = "Fedora-CoreOS-38"
  coe                   = "kubernetes"
  flavor                = "STD-2-4-10"
  master_flavor         = "STD-2-4-10"
  dns_nameserver        = "192.168.2.254"
  docker_storage_driver = "overlay2"
  docker_volume_size    = 7
  volume_driver         = "cinder"
  network_driver        = "calico"
  server_type           = "vm"
  external_network_id   = data.openstack_networking_network_v2.ext-network.id
  master_lb_enabled     = true
  floating_ip_enabled   = false

  labels = {
    cloud_provider_enabled           = "true"
    kube_tag                         = "v1.26.7-rancher1"
    master_lb_floating_ip_enabled    = "true"
    cinder_csi_enabled               = "true"
    container_runtime                = "containerd"
    containerd_version               = "1.6.20"
    containerd_tarball_sha256        = "1d86b534c7bba51b78a7eeb1b67dd2ac6c0edeb01c034cc5f590d5ccd824b416"
    cloud_provider_tag               = "v1.26.4"
    cinder_csi_plugin_tag            = "v1.26.4"
    k8s_keystone_auth_tag            = "v1.26.4"
    octavia_ingress_controller_tag   = "v1.26.4"
    coredns_tag                      = "1.10.1"
    csi_snapshotter_tag              = "v6.2.1"
    csi_attacher_tag                 = "v4.2.0"
    csi_resizer_tag                  = "v1.7.0"
    csi_provisioner_tag              = "v3.4.1"
    csi_node_driver_registrar_tag    = "v2.8.0"
    calico_ipv4pool_ipip             = "CrossSubnet"
    calico_tag                       = "v3.16.10"
  }
}

#-----------
## Create Cluster Template k8s-1.27.8
#-----------
resource "openstack_containerinfra_clustertemplate_v1" "k8s-1-27-8" {
  name                  = "k8s-1.27.8"
  image                 = "Fedora-CoreOS-38"
  coe                   = "kubernetes"
  flavor                = "STD-2-4-10"
  master_flavor         = "STD-2-4-10"
  dns_nameserver        = "192.168.2.254"
  docker_storage_driver = "overlay2"
  docker_volume_size    = 7
  volume_driver         = "cinder"
  network_driver        = "calico"
  server_type           = "vm"
  external_network_id   = data.openstack_networking_network_v2.ext-network.id
  master_lb_enabled     = true
  floating_ip_enabled   = false

  labels = {
    cloud_provider_enabled = "true"
    kube_tag = "v1.27.8-rancher2"
    master_lb_floating_ip_enabled = "true"
    cinder_csi_enabled = "true"
    container_runtime = "containerd"
    containerd_version = "1.6.28"
    containerd_tarball_sha256 = "f70736e52d61e5ad225f4fd21643b5ca1220013ab8b6c380434caeefb572da9b"
    cloud_provider_tag = "v1.27.3"
    cinder_csi_plugin_tag = "v1.27.3"
    k8s_keystone_auth_tag = "v1.27.3"
    coredns_tag = "1.10.1"
    csi_snapshotter_tag = "v6.2.1"
    csi_attacher_tag = "v4.2.0"
    csi_resizer_tag = "v1.7.0"
    csi_provisioner_tag = "v3.4.1"
    csi_node_driver_registrar_tag = "v2.8.0"
    calico_ipv4pool_ipip = "CrossSubnet"
    magnum_auto_healer_tag = "v1.27.3"
    calico_tag = "v3.24.6"
  }
}
