#-----------
## Create image cirros 061
#-----------

resource "openstack_images_image_v2" "cirros_061" {
  name             = "cirros-0.6.1"
  image_source_url = "https://download.cirros-cloud.net/0.6.1/cirros-0.6.1-x86_64-disk.img"
  container_format = "bare"
  disk_format      = "qcow2"

  properties = {
    os_version          = "0.6.1"
    hw_qemu_guest_agent = "yes"
    os_distro           = "Others"
    usage_type          = "common"
    os_admin_user       = "root"
  }
}